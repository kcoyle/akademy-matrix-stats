const http = require('http');
const url = require('url');
const client = require('prom-client');
const sdk = require('matrix-js-sdk');

const port = process.env.PORT || 8787;

// Create a Registry which registers the metrics
const register = new client.Registry()

// Add a default label which is added to all metrics
register.setDefaultLabels({
  app: 'kde-akademy-matrix-rooms'
})

const currentMatrixRoom1NumberGuage = new client.Gauge({
    name: 'akademy_room_1_count',
    help: 'Number of people in Akademy Matrix Room 1'
});

const currentMatrixRoom2NumberGuage = new client.Gauge({
    name: 'akademy_room_2_count',
    help: 'Number of people in Akademy Matrix Room 2'
});

const currentMatrixRoom3NumberGuage = new client.Gauge({
    name: 'akademy_attendees_count',
    help: 'Number of people in Akademy Attendees Matrix Room'
});

const currentMatrixRoom4NumberGuage = new client.Gauge({
    name: 'akademy_speakers_count',
    help: 'Number of people in Akademy Speakers Matrix Room'
});

const currentMatrixRoom5NumberGuage = new client.Gauge({
    name: 'akademy_volunteers_count',
    help: 'Number of people in Akademy Volunteers Matrix Room'
});

const currentMatrixRoom6NumberGuage = new client.Gauge({
    name: 'akademy_infodesk_count',
    help: 'Number of people in Akademy Infodesk Matrix Room'
});
  
// Register the guage
register.registerMetric(currentMatrixRoom1NumberGuage);
register.registerMetric(currentMatrixRoom2NumberGuage);
register.registerMetric(currentMatrixRoom3NumberGuage);
register.registerMetric(currentMatrixRoom4NumberGuage);
register.registerMetric(currentMatrixRoom5NumberGuage);
register.registerMetric(currentMatrixRoom6NumberGuage);

// Enable the collection of default metrics
// client.collectDefaultMetrics({ register })

const matrixClient = sdk.createClient({
    baseUrl: "https://kde.modular.im",
    accessToken: process.env.MATRIX_ACCESS_TOKEN,
    userId: "@kcoyle:kde.org"
});

matrixClient.startClient();

matrixClient.once('sync', function(state, prevState, res) {
    console.log(state); // state will be 'PREPARED' when the client is ready to use
});


// Define the HTTP server
const server = http.createServer(async (req, res) => {
  // Retrieve route from request object
  const route = url.parse(req.url).pathname

  // Determine Room Count
//   currentMatrixRoomNumberGuage.set(Math.floor(Math.random() * (50 - 35 + 1)) + 35);
  const rooms = matrixClient.getRooms();
  rooms.forEach(room => {
    const members = room.getJoinedMembers();
    
    if(room.name === 'Akademy Talks 1') {
        currentMatrixRoom1NumberGuage.set(members.length);
    }
    if(room.name === 'Akademy Talks 2') {
        currentMatrixRoom2NumberGuage.set(members.length);
    }
    if(room.name === 'Akademy Attendees') {
        currentMatrixRoom3NumberGuage.set(members.length);
    }
    if(room.name === 'Akademy Speakers') {
        currentMatrixRoom4NumberGuage.set(members.length);
    }
    if(room.name === 'Akademy Volunteers') {
        currentMatrixRoom5NumberGuage.set(members.length);
    }
    if(room.name === 'Akademy Infodesk') {
        currentMatrixRoom6NumberGuage.set(members.length);
    }
  });


  if (route === '/metrics') {
    // Return all metrics the Prometheus exposition format
    res.setHeader('Content-Type', register.contentType)
    register.metrics()
        .then(response => res.end(response));
  }
})

server.listen(port)